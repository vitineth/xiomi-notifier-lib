import * as fs from "fs";
import RethrownError from "./error/RethrownError";
import XiomiNotification from "./types/Notification";
import Notification from "./types/Notification";
import * as crypto from "crypto";
import { v4 } from 'uuid';
import axios from "axios";

/**
 * The url of the notifying endpoint to be used by default
 */
const BASE_URL = 'https://notifier.xiomi.org/notify';

/**
 * API handler for the xiomi-notifier system encapsulating authentication and sending
 */
export default class XiomiNotifier {

	/**
	 * Initialises a new API for the xiomi-notififer system, the key should be the contents of a PEM style private key
	 * (ie starts with BEGIN PRIVATE KEY)
	 * @param key the contents of a private key file for a registered client
	 * @param identifier the identifier associated with the private key supplied
	 * @param url the url of the notifier server to which requests should be dispatched
	 */
	constructor(key: string, identifier: string, url?: string);
	/**
	 * Initialises a new API for the xiomi-notififer system, the keyfie should be the location of the key file on disk
	 * to be loaded
	 * @param key the location of a key file for the registered client
	 * @param identifier the identifier associated with the private key supplied
	 * @param url the url of the notifier server to which requests should be dispatched
	 */
	constructor(key: string, identifier: string, url?: string);
	/**
	 * Initialises a new API for the xiomi-notififer system, the keyfile should be the path to a private key file or the
	 * contents of a PEM style private key (ie starts with BEGIN PRIVATE KEY)
	 * @param _key the location of or the contents of a private key file for a registered client
	 * @param _identifier the identifier associated with the private key supplied
	 * @param _url the url of the notifier server to which requests should be dispatched
	 */
	constructor(
		private readonly _key: string,
		private _identifier: string,
		private _url: string = BASE_URL,
	) {
		if (!_key.startsWith("-----BEGIN PRIVATE KEY-----")) {
			try {
				this._key = fs.readFileSync(_key, { encoding: 'utf8' });
			} catch (e) {
				if (e instanceof Error)
					throw new RethrownError('Failed to load the key file from disk due to an error raised by fs', e);
				throw e;
			}
		}
	}

	/**
	 * Converts the notification to the hashable string in the format decribed in the API
	 * (https://gitlab.com/vitineth/xiomi-dispatcher#authentication)
	 * @param nonce the nonce being used for this request authentication
	 * @param notification the notification, as it will be delivered, without modification
	 * @private
	 */
	private static convertToHash(nonce: number, notification: XiomiNotification) {
		return `${ nonce }.${ notification.uid }.${ notification.correlation ?? '' }.${ notification.status }.${ notification.module }.${ notification.topic }.${ notification.summary }.${ notification.detail ?? '' }.${ notification.timestamp ?? 0 }.${ Object.entries(notification.properties ?? {}).map(([key, value]) => `${ key }.${ value }`) }`
	}

	/**
	 * Sends a notification to the dispatcher. This will fill in any missing request parameters as described in
	 * {@link XiomiNotification}, hash it according to the authentication API and deliver it to {@link XiomiNotifier#url}
	 * and handle error responses.
	 * @param notification the notification which should be delivered
	 */
	public async notify(notification: XiomiNotification) {
		const extended = {
			uid: String(v4()),
			timestamp: Math.floor(Date.now() / 1000),
			...notification,
		};
		const nonce = crypto.randomInt(281474976710654);
		const hash = this.hash(nonce, extended);

		const payload = JSON.stringify({
			...extended,
			authentication: {
				nonce,
				hash,
				identifier: this._identifier,
			},
		});

		const result = await axios.post(this._url, payload, {
			headers: {
				'Content-Type': 'application/json',
			},
			validateStatus: () => true,
		});

		if (result.status === 401) {
			throw new Error("Authentication Failed: confirm the private key and identifier match and are valid");
		}

		if (result.status === 400) {
			throw new Error("Bad Request: A body was not specified or was not the valid format");
		}

		if (result.status !== 200) {
			throw new Error(`Failed ${ result.status } ${ result.statusText }: Unknown error`);
		}
	}

	/**
	 * Hashes the notification and nonce according to the API using the private key supplied in the constructor and
	 * return the digital signature for this notification which can be included in the authentication.
	 * @param nonce the nonce to authenticate thi request
	 * @param notification the final notification content as it will be delivered to the API
	 * @private
	 */
	private hash(nonce: number, notification: XiomiNotification): string {
		const hashable = XiomiNotifier.convertToHash(nonce, notification);
		const signer = crypto.createSign('RSA-SHA256');
		signer.write(hashable);
		signer.end();
		return signer.sign(this._key, 'base64');
	}

}

/**
 * A single notification wrapper around {@link XiomiNotification} which creates a new instance and calls
 * {@link XiomiNotifier#notify} on it immediately, returning the generated promise
 * @param identifier the identifier associated with the private key supplied
 * @param keyOrFile the location of or the contents of a private key file for a registered client
 * @param notification the notification which should be delivered
 * @param url the url of the notifier server to which requests should be dispatched
 */
export function notify(identifier: string, keyOrFile: string, notification: Notification, url: string = BASE_URL) {
	return new XiomiNotifier(keyOrFile, identifier, url).notify(notification);
}