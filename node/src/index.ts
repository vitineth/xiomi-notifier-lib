import XiomiNotifier from "./XiomiNotifier";

export { default as RethrownError } from "./error/RethrownError";
export { default as XiomiNotification } from "./types/Notification";

export default XiomiNotifier;