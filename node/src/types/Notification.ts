/**
 * Represents a single notification to be transmitted to a device
 */
type XiomiNotification = {
	/**
	 * The unique identifier of this message, will be generated automatically if not specified
	 */
	uid?: string,
	/**
	 * The correlation UUID for this request if specified, this should onl be used if you need to link thi with a
	 * previous request
	 */
	correlation?: string,
	/**
	 * The status of the notification to be registered in the app
	 */
	status: 'NOTIFY' | 'ALERT' | 'CRITICAL' | 'RESOLVING' | 'RESOLVED' | 'UNKNOWN' | 'MESSAGE',
	/**
	 * The name of the module transmitting this request
	 */
	module: string,
	/**
	 * The topic of this message
	 */
	topic: string,
	/**
	 * The main line of the notification to be delivered, this should be short and succinct, more detail should be
	 * provided by `detail`
	 */
	summary: string,
	/**
	 * Additional detail about the request which should be included in the expanded information
	 */
	detail?: string,
	/**
	 * Additional key value pairs to be included in the request
	 */
	properties?: Record<string, string>
	/**
	 * The time at which this notification was sent, if not specified it will be filled automatically with the current
	 * time. This should be specified as the current unix time UTC in seconds
	 */
	timestamp?: number,
};

export default XiomiNotification;