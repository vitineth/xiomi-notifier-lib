/**
 * Represents an error that needs to be thrown as a result of an error but provide more information
 */
export default class RethrownError extends Error {

	/**
	 * The original error that caused this one to be thrown
	 * @private
	 */
	private readonly _source: Error;

	/**
	 * A new error that wraps an existing error with an extra message. This will include the stack trace from the
	 * previous error in the raw stack to not loose that information and the original source is available via
	 * {@link source} in case there is additional metadata
	 * @param message the additional message to add to this request
	 * @param source the source of the original message
	 */
	constructor(message: string, source: Error) {
		super(message);

		this.stack = (this.stack ?? '').split('\n').concat(
			['Source error: ' + source.message].concat(
				(source.stack ?? '').split('\n').slice(1),
			),
		).join('\n');
		this._source = source;
	}

	/**
	 * Returns the original error that caused this error to be thrown
	 */
	get source(): Error {
		return this._source;
	}
}