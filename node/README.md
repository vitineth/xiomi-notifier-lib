# xiomi-notifier-lib

The node implementation for the xiomi-notifier system. This exports a default class `XiomiNotifier` which is initialised with `new XiomiNotifier(keyOrFile: string, identifier: string, url?: string)` and you can then call `notifier.notify(notification: Notification)` to send a notification to devices. All types and documentation should be provided but see the main repo for more information about general functionaity. 
