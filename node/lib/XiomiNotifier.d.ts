import XiomiNotification from "./types/Notification";
import Notification from "./types/Notification";
/**
 * API handler for the xiomi-notifier system encapsulating authentication and sending
 */
export default class XiomiNotifier {
    private readonly _key;
    private _identifier;
    private _url;
    /**
     * Initialises a new API for the xiomi-notififer system, the key should be the contents of a PEM style private key
     * (ie starts with BEGIN PRIVATE KEY)
     * @param key the contents of a private key file for a registered client
     * @param identifier the identifier associated with the private key supplied
     * @param url the url of the notifier server to which requests should be dispatched
     */
    constructor(key: string, identifier: string, url?: string);
    /**
     * Initialises a new API for the xiomi-notififer system, the keyfie should be the location of the key file on disk
     * to be loaded
     * @param key the location of a key file for the registered client
     * @param identifier the identifier associated with the private key supplied
     * @param url the url of the notifier server to which requests should be dispatched
     */
    constructor(key: string, identifier: string, url?: string);
    /**
     * Converts the notification to the hashable string in the format decribed in the API
     * (https://gitlab.com/vitineth/xiomi-dispatcher#authentication)
     * @param nonce the nonce being used for this request authentication
     * @param notification the notification, as it will be delivered, without modification
     * @private
     */
    private static convertToHash;
    /**
     * Sends a notification to the dispatcher. This will fill in any missing request parameters as described in
     * {@link XiomiNotification}, hash it according to the authentication API and deliver it to {@link XiomiNotifier#url}
     * and handle error responses.
     * @param notification the notification which should be delivered
     */
    notify(notification: XiomiNotification): Promise<void>;
    /**
     * Hashes the notification and nonce according to the API using the private key supplied in the constructor and
     * return the digital signature for this notification which can be included in the authentication.
     * @param nonce the nonce to authenticate thi request
     * @param notification the final notification content as it will be delivered to the API
     * @private
     */
    private hash;
}
/**
 * A single notification wrapper around {@link XiomiNotification} which creates a new instance and calls
 * {@link XiomiNotifier#notify} on it immediately, returning the generated promise
 * @param identifier the identifier associated with the private key supplied
 * @param keyOrFile the location of or the contents of a private key file for a registered client
 * @param notification the notification which should be delivered
 * @param url the url of the notifier server to which requests should be dispatched
 */
export declare function notify(identifier: string, keyOrFile: string, notification: Notification, url?: string): Promise<void>;
//# sourceMappingURL=XiomiNotifier.d.ts.map