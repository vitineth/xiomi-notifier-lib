"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Represents an error that needs to be thrown as a result of an error but provide more information
 */
class RethrownError extends Error {
    /**
     * A new error that wraps an existing error with an extra message. This will include the stack trace from the
     * previous error in the raw stack to not loose that information and the original source is available via
     * {@link source} in case there is additional metadata
     * @param message the additional message to add to this request
     * @param source the source of the original message
     */
    constructor(message, source) {
        var _a, _b;
        super(message);
        this.stack = ((_a = this.stack) !== null && _a !== void 0 ? _a : '').split('\n').concat(['Source error: ' + source.message].concat(((_b = source.stack) !== null && _b !== void 0 ? _b : '').split('\n').slice(1))).join('\n');
        this._source = source;
    }
    /**
     * Returns the original error that caused this error to be thrown
     */
    get source() {
        return this._source;
    }
}
exports.default = RethrownError;
//# sourceMappingURL=RethrownError.js.map