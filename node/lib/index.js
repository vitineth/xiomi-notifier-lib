"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RethrownError = void 0;
const XiomiNotifier_1 = __importDefault(require("./XiomiNotifier"));
var RethrownError_1 = require("./error/RethrownError");
Object.defineProperty(exports, "RethrownError", { enumerable: true, get: function () { return __importDefault(RethrownError_1).default; } });
exports.default = XiomiNotifier_1.default;
//# sourceMappingURL=index.js.map