"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.notify = void 0;
const fs = __importStar(require("fs"));
const RethrownError_1 = __importDefault(require("./error/RethrownError"));
const crypto = __importStar(require("crypto"));
const uuid_1 = require("uuid");
const axios_1 = __importDefault(require("axios"));
/**
 * The url of the notifying endpoint to be used by default
 */
const BASE_URL = 'https://notifier.xiomi.org/notify';
/**
 * API handler for the xiomi-notifier system encapsulating authentication and sending
 */
class XiomiNotifier {
    /**
     * Initialises a new API for the xiomi-notififer system, the keyfile should be the path to a private key file or the
     * contents of a PEM style private key (ie starts with BEGIN PRIVATE KEY)
     * @param _key the location of or the contents of a private key file for a registered client
     * @param _identifier the identifier associated with the private key supplied
     * @param _url the url of the notifier server to which requests should be dispatched
     */
    constructor(_key, _identifier, _url = BASE_URL) {
        this._key = _key;
        this._identifier = _identifier;
        this._url = _url;
        if (!_key.startsWith("-----BEGIN PRIVATE KEY-----")) {
            try {
                this._key = fs.readFileSync(_key, { encoding: 'utf8' });
            }
            catch (e) {
                if (e instanceof Error)
                    throw new RethrownError_1.default('Failed to load the key file from disk due to an error raised by fs', e);
                throw e;
            }
        }
    }
    /**
     * Converts the notification to the hashable string in the format decribed in the API
     * (https://gitlab.com/vitineth/xiomi-dispatcher#authentication)
     * @param nonce the nonce being used for this request authentication
     * @param notification the notification, as it will be delivered, without modification
     * @private
     */
    static convertToHash(nonce, notification) {
        var _a, _b, _c, _d;
        return `${nonce}.${notification.uid}.${(_a = notification.correlation) !== null && _a !== void 0 ? _a : ''}.${notification.status}.${notification.module}.${notification.topic}.${notification.summary}.${(_b = notification.detail) !== null && _b !== void 0 ? _b : ''}.${(_c = notification.timestamp) !== null && _c !== void 0 ? _c : 0}.${Object.entries((_d = notification.properties) !== null && _d !== void 0 ? _d : {}).map(([key, value]) => `${key}.${value}`)}`;
    }
    /**
     * Sends a notification to the dispatcher. This will fill in any missing request parameters as described in
     * {@link XiomiNotification}, hash it according to the authentication API and deliver it to {@link XiomiNotifier#url}
     * and handle error responses.
     * @param notification the notification which should be delivered
     */
    notify(notification) {
        return __awaiter(this, void 0, void 0, function* () {
            const extended = Object.assign({ uid: String((0, uuid_1.v4)()), timestamp: Math.floor(Date.now() / 1000) }, notification);
            const nonce = crypto.randomInt(281474976710654);
            const hash = this.hash(nonce, extended);
            const payload = JSON.stringify(Object.assign(Object.assign({}, extended), { authentication: {
                    nonce,
                    hash,
                    identifier: this._identifier,
                } }));
            const result = yield axios_1.default.post(this._url, payload, {
                headers: {
                    'Content-Type': 'application/json',
                },
                validateStatus: () => true,
            });
            if (result.status === 401) {
                throw new Error("Authentication Failed: confirm the private key and identifier match and are valid");
            }
            if (result.status === 400) {
                throw new Error("Bad Request: A body was not specified or was not the valid format");
            }
            if (result.status !== 200) {
                throw new Error(`Failed ${result.status} ${result.statusText}: Unknown error`);
            }
        });
    }
    /**
     * Hashes the notification and nonce according to the API using the private key supplied in the constructor and
     * return the digital signature for this notification which can be included in the authentication.
     * @param nonce the nonce to authenticate thi request
     * @param notification the final notification content as it will be delivered to the API
     * @private
     */
    hash(nonce, notification) {
        const hashable = XiomiNotifier.convertToHash(nonce, notification);
        const signer = crypto.createSign('RSA-SHA256');
        signer.write(hashable);
        signer.end();
        return signer.sign(this._key, 'base64');
    }
}
exports.default = XiomiNotifier;
/**
 * A single notification wrapper around {@link XiomiNotification} which creates a new instance and calls
 * {@link XiomiNotifier#notify} on it immediately, returning the generated promise
 * @param identifier the identifier associated with the private key supplied
 * @param keyOrFile the location of or the contents of a private key file for a registered client
 * @param notification the notification which should be delivered
 * @param url the url of the notifier server to which requests should be dispatched
 */
function notify(identifier, keyOrFile, notification, url = BASE_URL) {
    return new XiomiNotifier(keyOrFile, identifier, url).notify(notification);
}
exports.notify = notify;
//# sourceMappingURL=XiomiNotifier.js.map