import axios from 'axios';
import * as fs from 'fs';
import XiomiNotifier from "../src";

const TEST_BASE_URL = 'https://notifier.xiomi.org/notify';
const TESTING_ONLY_PRIVATE_KEY = `-----BEGIN PRIVATE KEY-----
MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQDt7fvdCOWoqRdD
nskgOSQbbIJWExbC8i7UzXNLuqupvVSWgoA8biO++4u1TUxlIrHlkptyMaRDuozO
ReUlgB8R4N/Muh3mWNlYzJD+pdrBRb8RyDNg+CRPKVX3AA2a/jBFW1aZTCtCd9zu
BC1LNWQhmfg2C9vCdcIs3ZPFiHF4EP4IWpviHUIWIc4EXPBnufOykqJe69drdkMP
dBSQ4kMHjtsNjix9k7s8QlOgFPR0rHq3xXhi942e0xCsY4wdaE7W24V4Dw9HHiSG
TaKIiAXHAzdTa8TuCFoCTIqLd/KUuWhiz2q7wheK3XBGGXUbKsvA7vSVEV/wiLJn
XonnbMVrCb8Atd3FVFsdos21rBJ2pdKqB1JtRQyRJDc7ugDzwkLK4rZYGCIYOvIJ
XHxx6uOtmDT0v3LeLxG2H5tX5fGBOjw64DE9s7HPE89Ny/oR6SQ7zVLpSG3lxf0/
dKviAxuWr7s9mhgS0Bzx+CghpOgWG1SqR82zaDs8ybLfAAsngwb5JdTCkC8kzK/i
TgR0FBEmbRoLwZxzxd4U+4vlolqdI/Y4W3Fg0VEoiC3tJEgbvF4Y7M8OKauaGiZX
czHAIjCPKXPw9RHNK05xogjyb61i5ybhV4iHrelZSbZbhJo/s3D2tY8UOwyGdAPq
2W/9CQXKEC3B0SvKESet2cibg5j8zQIBAwKCAgEAnp6n6LCZGxtk178wwCYYEkhW
5Ay510wfOIj3h9HHxn44ZFcAKEltKf0HzjOIQ2x2mQxnoXZtgnxd3tlDblVqC+s/
3dFpRDs7kIhgqcPnK4PUtoV3lfrC33Dj+gAJEf7K2OePEN1yLE/onq1zh3jta7v6
zrKSgaPWyJO32QWg+rX+sDxn7BOBZBaJWD31mnv3zGHBlJ06R6QstPgNtewsr7SS
CQly/mJ80tbiarii+HL8eoOll0+zvzdgcu0IE5rfOeeuUAoKL2ltrt5sWwVZL1d6
N50t9AWRVt2xsk/3DdDwQd+cfSwPsej1hBD4vMcygJ9NuLY/9bB275Rb753Y8L1K
pYNjQRoNUQKdUmwmL86Hm90DCfQONidwG46whM6rSubpr5cuY8bnkthQPt7h4sVN
QvzW3tDllFxgmnzqJdH5RAdU1sYJB7axefjMEE6dwPBhHal9i+emEPM/ylpG6auR
2ybaAid2v167TjR8b+Dz77cVg0zKf3saLPunr/wH+W50GmBunMdwzpLgZao3VuPI
+bKXVjV8B+OJt/8wlsb7s0c8Uhawm8wvdanUq3iScE7JKIL2rBfaIaVeq6bYDKu9
4ZC2lXiF8d7zIsxVnrFivpJz+ILcy8x6odm0EW0yZuifLp3Ug7yjlOjcvrZHnD0P
j8JWHdEVl4fNm9aJ9SsCggEBAP4ZpT4WNVYsRh+jDyWXr0/OV9ELJxsvhKA+N6Jg
eUVfimCSba0L41hpjE+UiZ2Cc8IuK4d1tG+PXY8SS0EEwnUz9doIQikadlknq+3C
jGQx8hTeqqKrp8UWsKh1f9yx4uKqxb0Dwji44DCIyIEERIdE7u5PbWWAhVJAx+2k
E+sU6t9OqsfYzdFUaEXCrs4Sm9KGwlcrSqTVHsvlKju0d3urVqt0rpwyHivn19wc
HBkIIxsURoqXUhtjKPTMBstUsfQo2tPTTDU0jirMBSTjpKaVS6ZYecdGsaDUUz2u
5f9xpCFOKggIgvqho//tjAq4UMBCQh+NCecrdJlTLzvKGBECggEBAO+1YzKyrlca
3f8+quRBf6A46GrCtzvHu0jQ2sGXs0biUCfFYwhQnzgtIMhvif8OZa4S5Ly624Yp
+gX4JOmNhgoSXvspuWGJzSidaukFbQ8FqE/ikieF7CVtdV9Wsp7/VUu1ryLNTVwx
8rmGY9UpKcKh08beAQ+K+D7K05jri53mGCDX8Dewa9AyRCvxLMariETl0t6OJ9DO
6lpYJ2fdQ33RVMUiNxMWIKn+5Mls/m56TVuNCtqiRdr7/AZXt9yXyjOuQ8gsoMFQ
+KjQORhJUDZ9d9WCGSJXdfYqFYBdP5AzuDdSJEZFPd5s53w3UuziDvRUKA4dk3Bb
1fTBAoX/9P0CggEBAKlmbilkI47ILr/CChkPyjU0OotcxLzKWGrUJRbq+4OVBusM
SR4H7OWbst+4W75W99bJclpOeEpfk7S23NYDLE4io+awLBtm+ZDFHUksXZghTA3p
xxcdGoNkdcWjqpMhQexx2SitLCXQlXWwhatYLa+DSfQ085kAWOGAhUkYDUdjRz+J
xy/l3ouNmtksdIlhvTcEgY9yMcM4vzKYxtJ4T6fHjxz4dGghaXKaj+gSvWYFbLy4
LwcPjBJCG03dWdzjIU1wkeKM3XjNtByIA23tGG8OMm7lpoTZy8CNjNPJ7qpLwsDe
xrAFrKcWbVVJCAcliyrW1r+zW+9yTbuMyifcEAsCggEBAJ/OQiHMdDoR6VTUce2A
/8Al8EcseifafNs15yu6d4SW4BqDl1rgaiVzazBKW/9e7nQMmH3R567Gpq6lbfEJ
BAa26fzGe5Zb3hsTnJtY819ZGt/sYW+unW5I+Oo5zGn/jjJ5H2yI3j12odEEQo4b
cSxr4oSUALUHUCncjRCdB76ZZWs6oCUgR+Ahgsf2HdnHsC3ujJRexTXfRublb5qT
glPg4y4Wz2IOwHFUmIZIqZ78M5JeBzxsLpH9Uq7lJT26hs0e19rIayuLUHCK0Lrb
is7+T+OsEMGPo/lxY6ro1QrNJXo2wtmDfpRImlLPjJ3sCfg4GrQTt6A9OU3WAa6q
o1MCggEAUcvvNB2j8coLEqDHwnCD5o7rV4Ms90/ao5l5kOcD0HlduYm5nxGXqSM7
x54PVexTuhhIJwBugoQQVLER1Io1+MfSoeTWsp8VlGejUf6CjWm8jnXBxY0eLmjW
u56J/7WabRzm6T0XYNyIYmwIzZsKjD7qjjrfsAcy3uwK89HPMekCd6KhjbrYgTtH
AK3Mb/xhUZuUTI+7bZznzALNw7FR9lxOULxTsEPJsl+JhAXpVOwB77KVvuDaT96y
Ho1oc7buhI7FkMC30+DHXHPE29ptbmbFbnocCdYN/SwVVZdvETWqhaXa8xqtiVKp
W0sxhJfSCLo8qVLD91JeGySpAthabQ==
-----END PRIVATE KEY-----`

jest.mock('axios');
jest.mock('fs');

beforeEach(() => {
	jest.resetAllMocks();
})

test('should load key from file when reference is passed', () => {
	// @ts-ignore
	fs.readFileSync.mockResolvedValue('keyfile');
	new XiomiNotifier('./keyfile.pem', '');
	expect(fs.readFileSync).toHaveBeenCalledTimes(1);
});

test('should not load key from file when start is valid', () => {
	// @ts-ignore
	fs.readFileSync.mockResolvedValue('keyfile');
	new XiomiNotifier('-----BEGIN PRIVATE KEY-----', '');
	expect(fs.readFileSync).toHaveBeenCalledTimes(0);

});

test('should throw rethrown error if file path is not valid', () => {
	expect(() => {
		// @ts-ignore
		fs.readFileSync.mockImplementation(() => {
			throw new Error("Invalid")
		});
		new XiomiNotifier('invalidfile', 'identifier');
	}).toThrow('Failed to load the key file from disk due to an error raised by fs')
});

test('should fill in random uid if not specified', () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 200 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07');
	notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		timestamp: 0,
	});


	expect(axios.post).toHaveBeenCalledTimes(1);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][0]).toEqual(TEST_BASE_URL);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][1]).toContain("\"uid\":\"");
});

test('should fill in timestamp if not specified', () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 200 });
	const now = Math.floor(Date.now() / 1000);
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07');
	notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		uid: '',
		correlation: '',
	});

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][0]).toEqual(TEST_BASE_URL);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][1]).toContain("\"timestamp\":" + now);
});

test('should submit request with hash and identifier to api', () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 200 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07');
	notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		uid: '',
		detail: '',
		correlation: '',
	});

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][0]).toEqual(TEST_BASE_URL);
	const obj = JSON.parse((axios.post as jest.MockedFn<any>).mock.calls[0][1] as string);
	expect(obj).toHaveProperty('authentication');
	expect(obj.authentication).toHaveProperty('nonce');
	expect(obj.authentication).toHaveProperty('hash');
	expect(obj.authentication).toHaveProperty('identifier');
	expect(obj.authentication.identifier).toEqual('c4baf645-2dc3-40a1-8b8f-5c924cabff07');

});
test('supports changing api target', () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 200 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07', 'https://test-address');
	notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		uid: '',
	});

	expect(axios.post).toHaveBeenCalledTimes(1);
	expect((axios.post as jest.MockedFn<any>).mock.calls[0][0]).toEqual('https://test-address');
});

test('it handles 401 errors', async () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 401 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07', 'https://test-address');

	return expect(notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		uid: '',
	})).rejects.toThrow('Authentication Failed: confirm the private key and identifier match and are valid');
})

test('it handles 400 errors', async () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 400 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07', 'https://test-address');

	return expect(notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		uid: '',
	})).rejects.toThrow('Bad Request: A body was not specified or was not the valid format');
})

test('it handles non 200 errors', async () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 409, statusText: 'Conflict' });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07', 'https://test-address');

	return expect(notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		uid: '',
	})).rejects.toThrow('Failed 409 Conflict: Unknown error');
});

test('it resolves on 200 response', async () => {
	(axios.post as jest.MockedFn<any>).mockResolvedValue({ status: 200 });
	const notifier = new XiomiNotifier(TESTING_ONLY_PRIVATE_KEY, 'c4baf645-2dc3-40a1-8b8f-5c924cabff07', 'https://test-address');

	return expect(notifier.notify({
		status: 'NOTIFY',
		topic: '',
		summary: '',
		module: '',
		detail: '',
		correlation: '',
		uid: '',
	})).resolves.toBeUndefined();
});