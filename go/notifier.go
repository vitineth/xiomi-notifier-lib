package _go

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"io"
	"maps"
	"math/big"
	"net/http"
	"os"
	"reflect"
	"strings"
	"time"
)

type NotifierOptionKey string

const (
	keyUrl       NotifierOptionKey = "URL"
	keyKeyFile   NotifierOptionKey = "FILE"
	keyKeyString NotifierOptionKey = "KEY"
)

type NotificationStatus string

type NotifierOption struct {
	key   NotifierOptionKey
	value string
}

func WithUrl(url string) NotifierOption {
	return NotifierOption{
		key:   keyUrl,
		value: url,
	}
}

func WithKeyfile(file string) NotifierOption {
	return NotifierOption{
		key:   keyKeyFile,
		value: file,
	}
}

func WithRawKey(key string) NotifierOption {
	return NotifierOption{
		key:   keyKeyString,
		value: key,
	}
}

const (
	StatusNotify    NotificationStatus = "NOTIFY"
	StatusAlert     NotificationStatus = "ALERT"
	StatusCritical  NotificationStatus = "CRITICAL"
	StatusResolving NotificationStatus = "RESOLVING"
	StatusResolved  NotificationStatus = "RESOLVED"
	StatusUnknown   NotificationStatus = "UNKNOWN"
	StatusMessage   NotificationStatus = "MESSAGE"
)

type Notification struct {
	UID           *string            `json:"uid"`
	CorrelationID *string            `json:"correlation"`
	Status        NotificationStatus `json:"status"`
	Module        string             `json:"module"`
	Topic         string             `json:"topic"`
	Summary       string             `json:"summary"`
	Detail        *string            `json:"detail"`
	Properties    *map[string]string `json:"properties"`
	Timestamp     *int               `json:"timestamp"`
}

func (receiver Notification) toMap() map[string]interface{} {
	return map[string]interface{}{
		"uid":         receiver.UID,
		"correlation": receiver.CorrelationID,
		"status":      receiver.Status,
		"module":      receiver.Module,
		"topic":       receiver.Topic,
		"summary":     receiver.Summary,
		"detail":      receiver.Detail,
		"properties":  receiver.Properties,
		"timestamp":   receiver.Timestamp,
	}
}

type Notifier struct {
	baseUrl    string
	identifier string
	key        *rsa.PrivateKey
}

func New(identifier string, options ...NotifierOption) (*Notifier, error) {
	var key *string = nil
	var url = "https://notifier.xiomi.org/notify"
	for _, option := range options {
		if option.key == keyKeyString {
			key = &option.value
		} else if option.key == keyKeyFile {
			data, err := os.ReadFile(option.value)
			if err != nil {
				return nil, fmt.Errorf("failed to read the specified key file %v: %w", option.value, err)
			}

			asString := string(data)
			key = &asString
		} else if option.key == keyUrl {
			url = option.value
		}
	}

	if key == nil {
		return nil, errors.New("no key was specified, please include one of WithKeyfile or WithRawKey")
	}

	pBlock, _ := pem.Decode([]byte(*key))
	if pBlock.Type != "RSA PRIVATE KEY" && pBlock.Type != "PRIVATE KEY" {
		return nil, fmt.Errorf("failed to parse key - not an RSA key, got type %v", pBlock.Type)
	}

	pkcs1Key, pkcs1Err := x509.ParsePKCS1PrivateKey(pBlock.Bytes)
	pkcs8Key, pkcs8Err := x509.ParsePKCS8PrivateKey(pBlock.Bytes)

	if pkcs8Err != nil && pkcs1Err != nil {
		return nil, fmt.Errorf("could not parse key as any type PKCS1(%w), PKCS8(%w)", pkcs1Err, pkcs8Err)
	}

	var privateKey *rsa.PrivateKey
	if pkcs1Key != nil {
		privateKey = pkcs1Key
	} else if pkcs8Key != nil {
		if cast, ok := pkcs8Key.(*rsa.PrivateKey); ok {
			privateKey = cast
		} else {
			return nil, errors.New("failed to parse the key as a valid rsa key, it was parsed as a pkcs8 key, but not an RSA one")
		}
	}

	if privateKey == nil {
		return nil, errors.New("key was not valid, could not be parsed")
	}

	return &Notifier{
		baseUrl:    url,
		identifier: identifier,
		key:        privateKey,
	}, nil
}

func convertToHashable(nonce int, notification Notification) string {
	properties := ""
	if notification.Properties != nil {
		entries := make([]string, len(*notification.Properties))
		index := 0
		for key, value := range *notification.Properties {
			entries[index] = fmt.Sprintf("%v.%v", key, value)
		}
		properties = strings.Join(entries, ",")
	}
	timestamp := 0
	if notification.Timestamp != nil {
		timestamp = *notification.Timestamp
	}
	detail := ""
	if notification.Detail != nil {
		detail = *notification.Detail
	}
	correlation := ""
	if notification.CorrelationID != nil {
		correlation = *notification.CorrelationID
	}
	return fmt.Sprintf(
		"%v.%v.%v.%v.%v.%v.%v.%v.%v.%v",
		nonce,
		*notification.UID,
		correlation,
		notification.Status,
		notification.Module,
		notification.Topic,
		notification.Summary,
		detail,
		timestamp,
		properties,
	)
}

func (notifier *Notifier) hash(nonce int, notification Notification) ([]byte, error) {
	hashable := convertToHashable(nonce, notification)
	msgHash := sha256.New()
	_, err := msgHash.Write([]byte(hashable))
	if err != nil {
		return nil, fmt.Errorf("failed to hash the message: %w", err)
	}

	hashSum := msgHash.Sum(nil)
	signature, err := rsa.SignPKCS1v15(rand.Reader, notifier.key, crypto.SHA256, hashSum)
	if err != nil {
		return nil, fmt.Errorf("failed to sign the message: %w", err)
	}

	return signature, nil
}

func (notifier *Notifier) Notify(notification Notification) error {
	if notification.UID == nil {
		random, err := uuid.NewRandom()
		if err != nil {
			return fmt.Errorf("failed to generate uuid %w", err)
		}
		uuidString := random.String()
		notification.UID = &uuidString
	}

	if notification.Timestamp == nil {
		nowSec := int(time.Now().Unix())
		notification.Timestamp = &nowSec
	}

	nonce, err := rand.Int(rand.Reader, big.NewInt(281474976710654))
	if err != nil {
		return fmt.Errorf("failed to generate nonce: %w", err)
	}

	signature, err := notifier.hash(int(nonce.Int64()), notification)
	if err != nil {
		return fmt.Errorf("failed to hash the messages: %w", err)
	}

	sigB64 := base64.StdEncoding.EncodeToString(signature)
	payload := map[string]interface{}{
		"authentication": map[string]interface{}{
			"nonce":      nonce.Int64(),
			"hash":       sigB64,
			"identifier": notifier.identifier,
		},
	}
	maps.Copy(payload, notification.toMap())

	for k, v := range payload {
		if v == nil || reflect.ValueOf(v).IsZero() {
			delete(payload, k)
		}
	}

	marhsalled, err := json.Marshal(payload)
	if err != nil {
		return fmt.Errorf("failed to convert payload to json: %w", err)
	}

	post, err := http.Post(notifier.baseUrl, "application/json", bytes.NewBuffer(marhsalled))
	if err != nil {
		return fmt.Errorf("failed to send post request to notifier server: %w", err)
	}

	if post.StatusCode < 200 || post.StatusCode > 299 {
		all, err := io.ReadAll(post.Body)
		if err != nil {
			return fmt.Errorf("the request failed, and also an error was thrown while trying to read the body: %v %v: %w", post.StatusCode, post.Status, err)
		}
		return fmt.Errorf("got a non-200 status code: %v %v: %v", post.StatusCode, post.Status, string(all))
	}

	return nil
}
