#!/bin/env sh
set -e


HELPTEXT=$(cat <<-END

xiomi-cli

NAME
    xiomi-cli, -- sh implementation of the xiomi-notifier/xiomi-dispatcher API with a limited feature set

SYNOPSIS
    xiomi-cli, [-u --uuid UUID ] [-c --correlation UUID] [-s --status STATUS] [-m --module STRING] [-t --topic STRING] [-n --summary STRING] [-d --detail STRING] [-o --timestamp NUMBER] [-i --identifier UUID] [-k --key-file FILE] [-h --help]

DESCRIPTION
    Allows you to dispatch notifications to clients using the notifier API. The command must specify at least STATUS, MODULE, TOPIC, SUMMARY, IDENTIFIER and KEY-FILE. See the online documentation for more on how these impact the notifications sent to client. The key file must point to a valid RSA private key stored on disk which can be parsed by openssl. A list of flags and their descriptions:

    -u --uid UUID (optional) 
            The unique ID for this message, this is optional and if left out one will be generated for you
    
    -c --correlation UUID (optional) 
            The correlational UUID for this to link it with others in the chain
    
    -s --status STATUS (required) 
            The status of the message, one of NOTIFY, ALERT, CRITICAL, RESOLVING, RESOLVED, UNKNOWN, MESSAGE
    
    -m --module STRING (required) 
            The name of the module that is sending this request
    
    -t --topic STRING (required) 
            The topic of this notification
    
    -n --summary STRING (required) 
            The main line of the noficiation to be delivered to devices
    
    -d --detail STRING (optional) 
            Additional detail about the request
    
    -o --timestamp NUMBER (optional) 
            The timestamp at which this message was sent, this will not delay sending, it will just pretend it was sent at a different time
    
    -i --identifier UUID (required) 
            The client ID for the client authenticating this request
    
    -k --key-file FILE (required) 
            The location of the RSA private key generated from the xiomi-dispatcher instance
    
    -h --help 
            Displays this message

FILES
    xiomi-cli.sh This file
END
)

POSITIONAL_ARGS=""

while [ $# -gt 0 ]; do
    case $1 in
        -u|--uuid)
            UUID=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -c|--correlation)
            CORRELATION=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -s|--status)
            STATUS=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -m|--module)
            MODULE=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -t|--topic)
            TOPIC=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -n|--summary)
            SUMMARY=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -d|--detail)
            DETAIL=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -o|--timestamp)
            TIMESTAMP="$2"
            shift
            shift
            ;;
        -i|--identifier)
            IDENTIFIER=$(echo "$2" | sed -e 's/"/\\"/g')
            shift
            shift
            ;;
        -k|--key-file)
            KEYFILE="$2"
            shift
            shift
            ;;
        -h|--help)
            echo "$HELPTEXT"
            exit 1
            ;;
        *)
            POSITIONAL_ARGS="${POSITIONAL_ARGS} $1"
            shift
            ;;
    esac
done

#   u uid
#   c correlation
# * s status
# * m module
# * t topic
# * n summary
#   d detail
#   o timestamp
# * i identifier
# * k key

if [ -z ${STATUS+x} ]; then
    echo "Must specify a status (-s|--status)"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${MODULE+x} ]; then
    echo "Must specify a module (-m|--module)"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${TOPIC+x} ]; then
    echo "Must specify a topic (-t|--topic)"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${SUMMARY+x} ]; then
    echo "Must specify a summary (-n|--summary)"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${IDENTIFIER+x} ]; then
    echo "Must specify a identifier (-i|--identifier)"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${KEYFILE+x} ]; then
    echo "Must specify a key file (-k|--key-file)"
    echo "$HELPTEXT"
    exit 1
elif [ ! -f "$KEYFILE" ] || [ ! -e "$KEYFILE" ]; then
    echo "Keyfile must be valid"
    echo "$HELPTEXT"
    exit 1
fi

if [ -z ${TIMESTAMP+x} ]; then
    TIMESTAMP=0
fi
if [ -z ${UUID+x} ]; then
    UUID="undefined"
fi

if [ "$STATUS" != "NOTIFY" ] && [ "$STATUS" != "ALERT" ] && [ "$STATUS" != "CRITICAL" ] && [ "$STATUS" != "RESOLVING" ] && [ "$STATUS" != "RESOLVED" ] && [ "$STATUS" != "UNKNOWN" ] && [ "$STATUS" != "MESSAGE" ]; then
    echo "Invalid status - must be one of the approved values (NOTIFY, ALERT, CRITICAL, RESOLVING, RESOLVED, UNKNOWN, MESSAGE)"
    echo "$HELPTEXT"
    exit 1
fi

NONCE=65536
while [ $NONCE -ge 65536 ]; do
  NONCE=1$(</dev/urandom tr -dc 0-9 | dd bs=5 count=1 2>/dev/null)
  NONCE=$((NONCE-100000))
done

HASHABLE="$NONCE.$UUID.$CORRELATION.$STATUS.$MODULE.$TOPIC.$SUMMARY.$DETAIL.$TIMESTAMP."
HASH=$(echo -n "$HASHABLE" | openssl dgst -binary -sha256 -sign $KEYFILE | openssl base64 -A)

UUID=$(echo "$UUID" | sed -e 's/"/\\"/g')
CORRELATION=$(echo "$CORRELATION" | sed -e 's/"/\\"/g')
STATUS=$(echo "$STATUS" | sed -e 's/"/\\"/g')
MODULE=$(echo "$MODULE" | sed -e 's/"/\\"/g')
TOPIC=$(echo "$TOPIC" | sed -e 's/"/\\"/g')
SUMMARY=$(echo "$SUMMARY" | sed -e 's/"/\\"/g')
DETAIL=$(echo "$DETAIL" | sed -e 's/"/\\"/g')
IDENTIFIER=$(echo "$IDENTIFIER" | sed -e 's/"/\\"/g')

PAYLOAD="{"
if [ "$UUID" != "undefined" ]; then
    PAYLOAD="$PAYLOAD\"uid\":\"$UUID\","
fi
if [ -n "${CORRELATION}" ]; then
    PAYLOAD="$PAYLOAD\"correlation\":\"$CORRELATION\","
fi
if [ -n "${DETAIL}" ]; then
    PAYLOAD="$PAYLOAD\"detail\":\"$DETAIL\","
fi
if [ "$TIMESTAMP" != 0 ]; then
    PAYLOAD="$PAYLOAD\"timestamp\":$TIMESTAMP,"
fi
PAYLOAD="$PAYLOAD\"status\":\"$STATUS\",\"module\":\"$MODULE\",\"topic\":\"$TOPIC\",\"summary\":\"$SUMMARY\",\"authentication\":{\"nonce\":$NONCE,\"identifier\":\"$IDENTIFIER\",\"hash\":\"$HASH\"}}"

curl -X POST -H "Content-Type: application/json" -d "$PAYLOAD" https://notifier.xiomi.org/notify
