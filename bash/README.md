xiomi-cli

NAME
    xiomi-cli, -- sh implementation of the xiomi-notifier/xiomi-dispatcher API with a limited feature set

SYNOPSIS
    xiomi-cli, [-u --uuid UUID ] [-c --correlation UUID] [-s --status STATUS] [-m --module STRING] [-t --topic STRING] [-n --summary STRING] [-d --detail STRING] [-o --timestamp NUMBER] [-i --identifier UUID] [-k --key-file FILE] [-h --help]

DESCRIPTION
    Allows you to dispatch notifications to clients using the notifier API. The command must specify at least STATUS, MODULE, TOPIC, SUMMARY, IDENTIFIER and KEY-FILE. See the online documentation for more on how these impact the notifications sent to client. The key file must point to a valid RSA private key stored on disk which can be parsed by openssl. A list of flags and their descriptions:

    -u --uid UUID (optional) 
            The unique ID for this message, this is optional and if left out one will be generated for you
    
    -c --correlation UUID (optional) 
            The correlational UUID for this to link it with others in the chain
    
    -s --status STATUS (required) 
            The status of the message, one of NOTIFY, ALERT, CRITICAL, RESOLVING, RESOLVED, UNKNOWN, MESSAGE
    
    -m --module STRING (required) 
            The name of the module that is sending this request
    
    -t --topic STRING (required) 
            The topic of this notification
    
    -n --summary STRING (required) 
            The main line of the noficiation to be delivered to devices
    
    -d --detail STRING (optional) 
            Additional detail about the request
    
    -o --timestamp NUMBER (optional) 
            The timestamp at which this message was sent, this will not delay sending, it will just pretend it was sent at a different time
    
    -i --identifier UUID (required) 
            The client ID for the client authenticating this request
    
    -k --key-file FILE (required) 
            The location of the RSA private key generated from the xiomi-dispatcher instance
    
    -h --help 
            Displays this message

FILES
    xiomi-cli.sh This file
